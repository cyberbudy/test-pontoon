# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-25 15:24+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: apps/administration/api/permissions.py:20
msgid "You need permission to observe for managing."
msgstr ""

#: apps/administration/api/permissions.py:33
msgid "You need at least investor permissions."
msgstr ""

#: apps/administration/api/permissions.py:48
msgid "You need director's permissions."
msgstr ""

#: apps/administration/api/permissions.py:61
msgid "Administration is not active yet."
msgstr ""

#: apps/administration/api/permissions.py:71
msgid "Pin code is not verificated."
msgstr ""

#: apps/administration/api/permissions.py:78
msgid "You have already voted for this proposal"
msgstr ""

#: apps/administration/api/serializers.py:42
#: apps/wallets/geopay/serializers.py:48
#: apps/wallets/wayforpay/serializers.py:48
msgid "Invalid pin code"
msgstr ""

#: apps/administration/api/serializers.py:166
msgid "Votes must be positive"
msgstr ""

#: apps/administration/api/serializers.py:263
msgid "Too many dices."
msgstr ""

#: apps/administration/api/serializers.py:272
msgid "You cannot create variants during voting"
msgstr ""

#: apps/administration/api/views.py:52
msgid "Pin code verification"
msgstr ""

#: apps/administration/api/views.py:54
msgid "Pin code successfully entered on administration verification"
msgstr ""

#: apps/administration/api/views.py:74
msgid "Proposal moderated header"
msgstr ""

#: apps/administration/api/views.py:75
msgid "Proposal moderated text"
msgstr ""

#: apps/administration/api/views.py:84
msgid "Proposal created but not moderated header"
msgstr ""

#: apps/administration/api/views.py:85
msgid "Proposal created but not moderated text"
msgstr ""

#: apps/administration/api/views.py:203
msgid "Votes saved header"
msgstr ""

#: apps/administration/api/views.py:204
msgid "Votes saved text"
msgstr ""

#: apps/administration/apps.py:7
msgid "administration"
msgstr ""

#: apps/administration/const.py:12
msgid "Moderated"
msgstr ""

#: apps/administration/const.py:13
msgid "Not views"
msgstr ""

#: apps/administration/const.py:14
msgid "Viewed"
msgstr ""

#: apps/administration/const.py:15 apps/staff/const.py:17
msgid "Canceled"
msgstr ""

#: apps/administration/models.py:30 apps/menu/models.py:23
#: apps/pages/models.py:34 apps/pages/models.py:75 apps/trade/models.py:42
msgid "Title"
msgstr ""

#: apps/administration/models.py:31 apps/pages/admin.py:24
#: apps/pages/models.py:35
msgid "Content"
msgstr ""

#: apps/administration/models.py:34
msgid "Moderation status"
msgstr ""

#: apps/administration/models.py:39 apps/auctions/models.py:93
#: apps/listing/models.py:43
msgid "Author"
msgstr ""

#: apps/administration/models.py:43
msgid "Moderated by"
msgstr ""

#: apps/administration/models.py:51 apps/administration/models.py:95
msgid "Proposal"
msgstr ""

#: apps/administration/models.py:52
msgid "Proposals"
msgstr ""

#: apps/administration/models.py:92 apps/chat/models.py:68
#: apps/pages/models.py:76
msgid "Text"
msgstr ""

#: apps/administration/models.py:100 apps/administration/models.py:144
msgid "Proposal variant"
msgstr ""

#: apps/administration/models.py:101
msgid "Proposal variants"
msgstr ""

#: apps/administration/models.py:137 apps/auctions/models.py:181
#: apps/staff/models/users.py:129
msgid "Dices"
msgstr ""

#: apps/administration/models.py:140 apps/auctions/models.py:157
#: apps/auctions/models.py:187 apps/listing/models.py:111
#: apps/staff/models/balances.py:31 apps/staff/models/balances.py:65
#: apps/staff/models/requests.py:66 apps/staff/models/requests.py:130
#: apps/staff/models/transactions.py:33 apps/statistics/models.py:36
#: apps/statistics/models.py:111 apps/trade/models.py:415
#: apps/wallets/models.py:20
msgid "User"
msgstr ""

#: apps/administration/models.py:149
msgid "Proposal variant vote"
msgstr ""

#: apps/administration/models.py:150
msgid "Proposal variant votes"
msgstr ""

#: apps/auctions/aggregates.py:112 apps/auctions/aggregates.py:131
#: apps/auctions/aggregates.py:134
msgid "Auction {} results.xlsx"
msgstr ""

#: apps/auctions/api/permissions.py:17
msgid "You can have at maximum 3 lots."
msgstr ""

#: apps/auctions/api/permissions.py:31
msgid "You can create a lot not later than 3 days before auction ends"
msgstr ""

#: apps/auctions/api/serializers.py:83 apps/auctions/models.py:164
msgid "Bet"
msgstr ""

#: apps/auctions/api/serializers.py:112
msgid "Not enough money to make a bet"
msgstr ""

#: apps/auctions/api/serializers.py:119
msgid "Bet value is must be higher then the previous one"
msgstr ""

#: apps/auctions/api/serializers.py:124
msgid "Bet value is must be higher then the minimum one"
msgstr ""

#: apps/auctions/api/views.py:53
msgid "Bet canceled header"
msgstr ""

#: apps/auctions/api/views.py:54
msgid "Bet canceled text"
msgstr ""

#: apps/auctions/api/views.py:135
msgid "Lot is created header"
msgstr ""

#: apps/auctions/api/views.py:136
msgid "Lot is created text"
msgstr ""

#: apps/auctions/api/views.py:169 apps/auctions/tests/test_views.py:94
msgid "Bet made header"
msgstr ""

#: apps/auctions/api/views.py:170
msgid "Bet made text"
msgstr ""

#: apps/auctions/apps.py:7 apps/auctions/models.py:54 apps/pages/const.py:13
msgid "Auctions"
msgstr ""

#: apps/auctions/const.py:8 apps/staff/api/serializers.py:208
msgid "Username"
msgstr ""

#: apps/auctions/const.py:9
msgid "Win dices"
msgstr ""

#: apps/auctions/const.py:10
msgid "Payed"
msgstr ""

#: apps/auctions/const.py:11
msgid "Previous dices"
msgstr ""

#: apps/auctions/const.py:12
msgid "New dices"
msgstr ""

#: apps/auctions/const.py:13
msgid "Was role"
msgstr ""

#: apps/auctions/const.py:14
msgid "New role"
msgstr ""

#: apps/auctions/entities.py:155
msgid "Your bet was interrupted by {} with a bet {}"
msgstr ""

#: apps/auctions/models.py:31
msgid "Started at"
msgstr ""

#: apps/auctions/models.py:32
msgid "Ends at"
msgstr ""

#: apps/auctions/models.py:34 apps/auctions/models.py:114
msgid "Is finished"
msgstr ""

#: apps/auctions/models.py:37
msgid "Celery task id"
msgstr ""

#: apps/auctions/models.py:41
msgid "Send results"
msgstr ""

#: apps/auctions/models.py:45
msgid "Auction currency"
msgstr ""

#: apps/auctions/models.py:53 apps/auctions/models.py:110
#: apps/auctions/models.py:191
msgid "Auction"
msgstr ""

#: apps/auctions/models.py:62 apps/auctions/tests/test_models.py:30
#, python-brace-format
msgid "Auction {starts_at} - {ends_at}"
msgstr ""

#: apps/auctions/models.py:96
msgid "Dices amount"
msgstr ""

#: apps/auctions/models.py:98
msgid "Minimum bet"
msgstr ""

#: apps/auctions/models.py:102
msgid "Winner"
msgstr ""

#: apps/auctions/models.py:106
msgid "Last bet"
msgstr ""

#: apps/auctions/models.py:120 apps/auctions/models.py:153
msgid "Lot"
msgstr ""

#: apps/auctions/models.py:121
msgid "Lots"
msgstr ""

#: apps/auctions/models.py:125 apps/auctions/tests/test_models.py:96
#, python-brace-format
msgid "Lot {dices} min price {min_price}"
msgstr ""

#: apps/auctions/models.py:148 apps/auctions/models.py:183
msgid "Bet value"
msgstr ""

#: apps/auctions/models.py:165
msgid "Bets"
msgstr ""

#: apps/auctions/models.py:169 apps/auctions/tests/test_models.py:135
msgid "Bet value: {}"
msgstr ""

#: apps/auctions/models.py:195
msgid "Auctions lot"
msgstr ""

#: apps/auctions/models.py:200
msgid "Dice transaction"
msgstr ""

#: apps/auctions/models.py:201
msgid "Dice transactions"
msgstr ""

#: apps/auctions/use_cases.py:29
msgid "No active auctions"
msgstr ""

#: apps/chat/api/serializers.py:59
msgid "You're banned."
msgstr ""

#: apps/chat/apps.py:7
msgid "chat"
msgstr ""

#: apps/chat/const.py:13
msgid "Chat on main page"
msgstr ""

#: apps/chat/const.py:14
msgid "Chat on administration page"
msgstr ""

#: apps/chat/const.py:19
msgid "Input"
msgstr ""

#: apps/chat/const.py:20
msgid "Output"
msgstr ""

#: apps/chat/models.py:24
msgid "Message text"
msgstr ""

#: apps/chat/models.py:27
msgid "From"
msgstr ""

#: apps/chat/models.py:32
msgid "Chat type"
msgstr ""

#: apps/chat/models.py:36 apps/statistics/models.py:105
msgid "Message"
msgstr ""

#: apps/chat/models.py:37
msgid "Messages"
msgstr ""

#: apps/chat/models.py:60 apps/trade/models.py:220
msgid "User from"
msgstr ""

#: apps/chat/models.py:64 apps/trade/models.py:228
msgid "User to"
msgstr ""

#: apps/chat/models.py:69
msgid "Is read"
msgstr ""

#: apps/chat/models.py:71
msgid "Message type"
msgstr ""

#: apps/chat/models.py:78
msgid "Private message"
msgstr ""

#: apps/chat/models.py:79 apps/menu/templatetags/menu.py:51
msgid "Private messages"
msgstr ""

#: apps/listing/api/permissions.py:12
msgid "You don't have enough money."
msgstr ""

#: apps/listing/api/serializers.py:68
msgid "{} is already on the website"
msgstr ""

#: apps/listing/api/serializers.py:78
msgid "Not enough money"
msgstr ""

#: apps/listing/api/serializers.py:95
msgid "You need {} but only have {}"
msgstr ""

#: apps/listing/api/views.py:69
msgid ""
"Дякуємо! Ваша заявка на модерації. Протягом 48 годин вона буде розглянута. "
"При позитивному рішенні монета буде додана в список претендентів для "
"лістингу!."
msgstr ""

#: apps/listing/api/views.py:108
msgid "Votes has been added"
msgstr ""

#: apps/listing/api/views.py:146
msgid "Votes have been revoked successfully"
msgstr ""

#: apps/listing/apps.py:7
msgid "listing"
msgstr ""

#: apps/listing/const.py:10 apps/staff/const.py:65
msgid "Draft"
msgstr ""

#: apps/listing/const.py:11
msgid "Published"
msgstr ""

#: apps/listing/const.py:12
msgid "Cancelled"
msgstr ""

#: apps/listing/const.py:13 apps/staff/const.py:18
msgid "Approved"
msgstr ""

#: apps/listing/models.py:23
msgid "Coin title"
msgstr ""

#: apps/listing/models.py:25
msgid "Coin short title"
msgstr ""

#: apps/listing/models.py:27
msgid "Coin website"
msgstr ""

#: apps/listing/models.py:29
msgid "Coin telegram"
msgstr ""

#: apps/listing/models.py:32
msgid "Coin status"
msgstr ""

#: apps/listing/models.py:36
msgid "Description"
msgstr ""

#: apps/listing/models.py:39
msgid "Technical details"
msgstr ""

#: apps/listing/models.py:50
msgid "Listing coin"
msgstr ""

#: apps/listing/models.py:51
msgid "Listing coins"
msgstr ""

#: apps/listing/models.py:91 apps/listing/models.py:98
#: apps/listing/models.py:99
msgid "Coin social page url"
msgstr ""

#: apps/listing/models.py:93 apps/listing/models.py:107
msgid "Coin"
msgstr ""

#: apps/listing/models.py:113
msgid "Votes"
msgstr ""

#: apps/listing/models.py:116
msgid "Created"
msgstr ""

#: apps/listing/models.py:120
msgid "Coin vote"
msgstr ""

#: apps/listing/models.py:121
msgid "Coin votes"
msgstr ""

#: apps/mailer/admin.py:24
msgid "Context"
msgstr ""

#: apps/mailer/aggregates.py:107
msgid "No MailTemplate for \"{}\" event"
msgstr ""

#: apps/mailer/apps.py:7
msgid "mailer"
msgstr ""

#: apps/mailer/const.py:12
msgid "draft"
msgstr ""

#: apps/mailer/const.py:13
msgid "Sent"
msgstr ""

#: apps/mailer/const.py:14 apps/staff/const.py:67
msgid "Failed"
msgstr ""

#: apps/mailer/models.py:37 apps/pages/models.py:99
#: apps/staff/models/users.py:37
msgid "Name"
msgstr ""

#: apps/mailer/models.py:40 apps/mailer/models.py:75
msgid "Event"
msgstr ""

#: apps/mailer/models.py:44 apps/mailer/models.py:66
msgid "Subject"
msgstr ""

#: apps/mailer/models.py:45
msgid "HTML text"
msgstr ""

#: apps/mailer/models.py:47 apps/mailer/models.py:68
msgid "Plain text"
msgstr ""

#: apps/mailer/models.py:51
msgid "Mail template"
msgstr ""

#: apps/mailer/models.py:52
msgid "Mail templates"
msgstr ""

#: apps/mailer/models.py:67
msgid "HTML"
msgstr ""

#: apps/mailer/models.py:71
msgid "Email from"
msgstr ""

#: apps/mailer/models.py:73
msgid "Letter recipients"
msgstr ""

#: apps/mailer/models.py:79 apps/staff/models/transactions.py:59
msgid "Status"
msgstr ""

#: apps/mailer/models.py:86
msgid "Mail letter"
msgstr ""

#: apps/mailer/models.py:87
msgid "Mail letters"
msgstr ""

#: apps/mailer/models.py:96 apps/mailer/models.py:113
msgid "Letter"
msgstr ""

#: apps/mailer/models.py:100 apps/mailer/models.py:103
msgid "Letter attachment"
msgstr ""

#: apps/mailer/models.py:104
msgid "Letter attachments"
msgstr ""

#: apps/mailer/models.py:108
msgid "Log message"
msgstr ""

#: apps/mailer/models.py:110 apps/statistics/models.py:107
msgid "Traceback"
msgstr ""

#: apps/mailer/models.py:117 apps/statistics/models.py:116
msgid "Log"
msgstr ""

#: apps/mailer/models.py:118 apps/statistics/models.py:117
msgid "Logs"
msgstr ""

#: apps/mailer/tasks.py:66
msgid "Letter with id {} is not exists or was already sent."
msgstr ""

#: apps/menu/apps.py:7
msgid "menu"
msgstr ""

#: apps/menu/const.py:12
msgid "Header"
msgstr ""

#: apps/menu/const.py:13
msgid "Footer"
msgstr ""

#: apps/menu/models.py:25
msgid "Link"
msgstr ""

#: apps/menu/models.py:35
msgid "Position"
msgstr ""

#: apps/menu/models.py:39
msgid "Menu item"
msgstr ""

#: apps/menu/models.py:40
msgid "Menu items"
msgstr ""

#: apps/menu/templatetags/menu.py:47
msgid "Personal data"
msgstr ""

#: apps/menu/templatetags/menu.py:48
msgid "Finances"
msgstr ""

#: apps/menu/templatetags/menu.py:49
msgid "Trade history"
msgstr ""

#: apps/menu/templatetags/menu.py:50
msgid "Transaction history"
msgstr ""

#: apps/menu/templatetags/menu.py:53
msgid "Administration page"
msgstr ""

#: apps/notifications/apps.py:7
msgid "Notifications"
msgstr ""

#: apps/notifications/sms.py:56
msgid "Invalid response {} for {}"
msgstr ""

#: apps/pages/admin.py:27
msgid "SEO"
msgstr ""

#: apps/pages/api/views.py:26 apps/pages/public_api/views.py:20
msgid "Thanks for contact us"
msgstr ""

#: apps/pages/apps.py:7
msgid "pages"
msgstr ""

#: apps/pages/const.py:10 apps/pages/models.py:123 apps/pages/models.py:124
msgid "FAQ"
msgstr ""

#: apps/pages/const.py:11 apps/trade/apps.py:7
msgid "Trade"
msgstr ""

#: apps/pages/const.py:12
msgid "Index"
msgstr ""

#: apps/pages/const.py:14
msgid "Listing"
msgstr ""

#: apps/pages/models.py:37
msgid "SEO title"
msgstr ""

#: apps/pages/models.py:40
msgid "SEO keywords"
msgstr ""

#: apps/pages/models.py:43
msgid "SEO description"
msgstr ""

#: apps/pages/models.py:47
msgid "Slug"
msgstr ""

#: apps/pages/models.py:50
msgid "Template"
msgstr ""

#: apps/pages/models.py:55
msgid "Page"
msgstr ""

#: apps/pages/models.py:56
msgid "Pages"
msgstr ""

#: apps/pages/models.py:80
msgid "Field ordering"
msgstr ""

#: apps/pages/models.py:83
msgid "Icon class"
msgstr ""

#: apps/pages/models.py:88
msgid "Advantage"
msgstr ""

#: apps/pages/models.py:89
msgid "Advantages"
msgstr ""

#: apps/pages/models.py:102 apps/staff/const.py:48
#: apps/staff/models/users.py:153
msgid "Phone"
msgstr ""

#: apps/pages/models.py:104
msgid "Email"
msgstr ""

#: apps/pages/models.py:105 apps/pages/models.py:118
msgid "Question"
msgstr ""

#: apps/pages/models.py:108 apps/pages/models.py:109
msgid "Contact us"
msgstr ""

#: apps/pages/models.py:119
msgid "Answer"
msgstr ""

#: apps/pages/models.py:132
msgid "Banner"
msgstr ""

#: apps/pages/models.py:136
msgid "Block header"
msgstr ""

#: apps/pages/models.py:139
msgid "Block test"
msgstr ""

#: apps/pages/models.py:142
msgid "Block image"
msgstr ""

#: apps/pages/models.py:147 apps/pages/models.py:150
msgid "Main page"
msgstr ""

#: apps/staff/admin.py:43
msgid "Request is approved"
msgstr ""

#: apps/staff/admin.py:45
msgid "Approve"
msgstr ""

#: apps/staff/admin.py:50
msgid "Request is canceled"
msgstr ""

#: apps/staff/admin.py:52
msgid "Cancel"
msgstr ""

#: apps/staff/admin.py:72
msgid "Personal info"
msgstr ""

#: apps/staff/admin.py:81
msgid "Verificated data"
msgstr ""

#: apps/staff/admin.py:97
msgid "Permissions"
msgstr ""

#: apps/staff/admin.py:104
msgid "Important dates"
msgstr ""

#: apps/staff/admin.py:169
msgid "Pin code changed"
msgstr ""

#: apps/staff/api/permissions.py:22
msgid "Your balance is held for {}. You cannot make any withdraw operations"
msgstr ""

#: apps/staff/api/permissions.py:44
msgid "You have already initialized two factor authorization."
msgstr ""

#: apps/staff/api/permissions.py:51
msgid "You don't have two factor authorization."
msgstr ""

#: apps/staff/api/serializers.py:133
msgid "Incorrect data format, should be YYYY-MM-DD"
msgstr ""

#: apps/staff/api/serializers.py:194
msgid "Invalid code"
msgstr ""

#: apps/staff/api/serializers.py:210 apps/staff/api/serializers.py:345
msgid "Password"
msgstr ""

#: apps/staff/api/serializers.py:235
msgid "Please enter a correct username and password."
msgstr ""

#: apps/staff/api/serializers.py:245
msgid "This account is inactive."
msgstr ""

#: apps/staff/api/serializers.py:273
msgid "Invalid token. Please make sure you have entered it correctly."
msgstr ""

#: apps/staff/api/serializers.py:281
msgid "Token"
msgstr ""

#: apps/staff/api/serializers.py:316
msgid "Entered token is not valid."
msgstr ""

#: apps/staff/api/serializers.py:336 apps/staff/forms/auth.py:87
msgid "Agree with licence rules"
msgstr ""

#: apps/staff/api/serializers.py:351
msgid "Password confirmation"
msgstr ""

#: apps/staff/api/serializers.py:353
msgid "Enter the same password as before, for verification."
msgstr ""

#: apps/staff/api/serializers.py:404 apps/staff/forms/auth.py:53
#: apps/staff/forms/auth.py:73
msgid "There is already a user with such username"
msgstr ""

#: apps/staff/api/serializers.py:417
msgid "The two password fields didn't match."
msgstr ""

#: apps/staff/api/views/auth.py:110
msgid "Header two factor auth enabled."
msgstr ""

#: apps/staff/api/views/auth.py:111
#: apps/staff/tests/views/test_auth_views.py:289
msgid "Text two factor auth enabled."
msgstr ""

#: apps/staff/api/views/auth.py:139
msgid "Header registration success."
msgstr ""

#: apps/staff/api/views/auth.py:140
msgid "Text registration success."
msgstr ""

#: apps/staff/api/views/cabinet.py:70
msgid "User verification request successfull header"
msgstr ""

#: apps/staff/api/views/cabinet.py:71
msgid "User verification request successfull text"
msgstr ""

#: apps/staff/api/views/cabinet.py:99
msgid "Header Enter the code you received on your phone."
msgstr ""

#: apps/staff/api/views/cabinet.py:102
msgid "Text Enter the code you received on your phone."
msgstr ""

#: apps/staff/api/views/cabinet.py:120
msgid "Phone is verified header"
msgstr ""

#: apps/staff/api/views/cabinet.py:121
msgid "Phone is verified text"
msgstr ""

#: apps/staff/api/views/cabinet.py:176 apps/staff/api/views/cabinet.py:201
#: apps/staff/api/views/cabinet.py:254
msgid "Invalid currency code"
msgstr ""

#: apps/staff/api/views/cabinet.py:285
msgid "Your request will be processed in 72 hours"
msgstr ""

#: apps/staff/apps.py:11
msgid "staff"
msgstr ""

#: apps/staff/const.py:16
msgid "Opened"
msgstr ""

#: apps/staff/const.py:19
msgid "Failed validation"
msgstr ""

#: apps/staff/const.py:29
msgid "EXCHANGE :: Can watch exchange managing"
msgstr ""

#: apps/staff/const.py:33
msgid "EXCHANGE :: Can create proposals and vote"
msgstr ""

#: apps/staff/const.py:37
msgid "EXCHANGE :: Can approve proposals"
msgstr ""

#: apps/staff/const.py:44
msgid "First name"
msgstr ""

#: apps/staff/const.py:45 apps/staff/models/users.py:147
msgid "Middle name"
msgstr ""

#: apps/staff/const.py:46
msgid "Last name"
msgstr ""

#: apps/staff/const.py:47 apps/staff/models/users.py:150
msgid "Date of birth"
msgstr ""

#: apps/staff/const.py:49 apps/staff/models/users.py:156
msgid "Car number"
msgstr ""

#: apps/staff/const.py:50 apps/staff/models/users.py:159
msgid "Mother last name"
msgstr ""

#: apps/staff/const.py:51 apps/staff/models/users.py:163
msgid "Passport data"
msgstr ""

#: apps/staff/const.py:52 apps/staff/models/users.py:166
msgid "Passport"
msgstr ""

#: apps/staff/const.py:53 apps/staff/models/users.py:169
msgid "Social link"
msgstr ""

#: apps/staff/const.py:54 apps/staff/models/users.py:172
msgid "Registration address"
msgstr ""

#: apps/staff/const.py:59
msgid "Replenish"
msgstr ""

#: apps/staff/const.py:60
msgid "Withdrawal"
msgstr ""

#: apps/staff/const.py:66
msgid "Confirmed"
msgstr ""

#: apps/staff/entities/transaction.py:48 apps/wallets/bitcoin/serializers.py:28
#: apps/wallets/cardano/serializers.py:28
#: apps/wallets/dogecoin/serializers.py:28
#: apps/wallets/ethereum/serializers.py:28
#: apps/wallets/karbowanec/serializers.py:36
#: apps/wallets/litecoin/serializers.py:28 apps/wallets/models.py:18
#: apps/wallets/nem/serializers.py:28
msgid "Address"
msgstr ""

#: apps/staff/entities/wallet.py:37
msgid "No balance for a user"
msgstr ""

#: apps/staff/entities/wallet.py:210
msgid "Not enough money on the balance. Required {} but have only {}"
msgstr ""

#: apps/staff/filtersets.py:22 apps/staff/filtersets.py:62
msgid "Closed  at greater"
msgstr ""

#: apps/staff/filtersets.py:27 apps/staff/filtersets.py:67
msgid "Closed  at less"
msgstr ""

#: apps/staff/forms/cabinet.py:25
msgid "New email address"
msgstr ""

#: apps/staff/forms/cabinet.py:26
msgid "Pin-code"
msgstr ""

#: apps/staff/forms/cabinet.py:45
msgid "PIN code is invalid"
msgstr ""

#: apps/staff/forms/cabinet.py:104 apps/staff/tests/test_forms.py:196
msgid "Passport image"
msgstr ""

#: apps/staff/forms/cabinet.py:114
msgid "File size is larger then 5mb"
msgstr ""

#: apps/staff/models/balances.py:27 apps/staff/models/transactions.py:42
msgid "Balance"
msgstr ""

#: apps/staff/models/balances.py:36 apps/staff/models/transactions.py:37
#: apps/trade/models.py:83 apps/trade/models.py:422 apps/wallets/models.py:24
msgid "Currency"
msgstr ""

#: apps/staff/models/balances.py:41 apps/staff/models/balances.py:61
msgid "User balance"
msgstr ""

#: apps/staff/models/balances.py:42 apps/staff/public_api/views.py:159
msgid "User balances"
msgstr ""

#: apps/staff/models/balances.py:46
msgid "{} {} {}"
msgstr ""

#: apps/staff/models/balances.py:69 apps/staff/models/requests.py:128
msgid "Reason"
msgstr ""

#: apps/staff/models/balances.py:71
msgid "Holding hours"
msgstr ""

#: apps/staff/models/balances.py:76
msgid "Balance holding"
msgstr ""

#: apps/staff/models/balances.py:77
msgid "Balance holdings"
msgstr ""

#: apps/staff/models/dividends_accural.py:13 shared/models.py:22
msgid "Created at"
msgstr ""

#: apps/staff/models/dividends_accural.py:17
msgid "Dividends accural"
msgstr ""

#: apps/staff/models/dividends_accural.py:18
msgid "Dividends accurals"
msgstr ""

#: apps/staff/models/requests.py:44
msgid "Field"
msgstr ""

#: apps/staff/models/requests.py:47
msgid "Old data"
msgstr ""

#: apps/staff/models/requests.py:50
msgid "New data"
msgstr ""

#: apps/staff/models/requests.py:54
msgid "Old image"
msgstr ""

#: apps/staff/models/requests.py:57
msgid "New image"
msgstr ""

#: apps/staff/models/requests.py:61 apps/staff/models/requests.py:125
msgid "Current status"
msgstr ""

#: apps/staff/models/requests.py:74
msgid "User verification request"
msgstr ""

#: apps/staff/models/requests.py:75
msgid "User verification requests"
msgstr ""

#: apps/staff/models/requests.py:138
msgid "Pin request"
msgstr ""

#: apps/staff/models/requests.py:139
msgid "Pin requests"
msgstr ""

#: apps/staff/models/transactions.py:46 apps/trade/models.py:199
#: apps/trade/models.py:377 apps/trade/models.py:427
msgid "Sum"
msgstr ""

#: apps/staff/models/transactions.py:49
msgid "Fee"
msgstr ""

#: apps/staff/models/transactions.py:53
msgid "Transaction type"
msgstr ""

#: apps/staff/models/transactions.py:56 apps/trade/models.py:212
msgid "Closed at"
msgstr ""

#: apps/staff/models/transactions.py:64
msgid "Status change reason"
msgstr ""

#: apps/staff/models/transactions.py:68
msgid "Transaction meta data"
msgstr ""

#: apps/staff/models/transactions.py:74
msgid "Transaction"
msgstr ""

#: apps/staff/models/transactions.py:75
msgid "Transactions"
msgstr ""

#: apps/staff/models/users.py:42
msgid "Dices lower"
msgstr ""

#: apps/staff/models/users.py:45
msgid "Dices upper"
msgstr ""

#: apps/staff/models/users.py:48
msgid "Role label color"
msgstr ""

#: apps/staff/models/users.py:52
msgid "Order commision in %"
msgstr ""

#: apps/staff/models/users.py:56
msgid "Role privileges level"
msgstr ""

#: apps/staff/models/users.py:57
msgid "The higher -> the more privileges user has."
msgstr ""

#: apps/staff/models/users.py:62
msgid "Permissins"
msgstr ""

#: apps/staff/models/users.py:67
msgid "Required fields"
msgstr ""

#: apps/staff/models/users.py:72
msgid "User role"
msgstr ""

#: apps/staff/models/users.py:73
msgid "User roles"
msgstr ""

#: apps/staff/models/users.py:104
msgid "Dices range"
msgstr ""

#: apps/staff/models/users.py:112
msgid "username"
msgstr ""

#: apps/staff/models/users.py:116
msgid "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
msgstr ""

#: apps/staff/models/users.py:120
msgid "A user with that username already exists."
msgstr ""

#: apps/staff/models/users.py:125
msgid "Pin code"
msgstr ""

#: apps/staff/models/users.py:127
msgid "email address"
msgstr ""

#: apps/staff/models/users.py:131
msgid "Not usa citizen"
msgstr ""

#: apps/staff/models/users.py:134
msgid "Previous email"
msgstr ""

#: apps/staff/models/users.py:136
msgid "Is banned"
msgstr ""

#: apps/staff/models/users.py:138
msgid "Role"
msgstr ""

#: apps/staff/models/users.py:141
msgid "Last administration pin code entered"
msgstr ""

#: apps/staff/models/users.py:178
msgid "First name is verified"
msgstr ""

#: apps/staff/models/users.py:181
msgid "Middle name is verified"
msgstr ""

#: apps/staff/models/users.py:184
msgid "Last name is verified"
msgstr ""

#: apps/staff/models/users.py:187
msgid "Date of birth is verified"
msgstr ""

#: apps/staff/models/users.py:190
msgid "Phone is verified"
msgstr ""

#: apps/staff/models/users.py:193
msgid "Car number is verified"
msgstr ""

#: apps/staff/models/users.py:196
msgid "Mother last name is verified"
msgstr ""

#: apps/staff/models/users.py:199
msgid "Passport data is verified"
msgstr ""

#: apps/staff/models/users.py:202
msgid "Passport is verified"
msgstr ""

#: apps/staff/models/users.py:205
msgid "Social link is verified"
msgstr ""

#: apps/staff/models/users.py:208
msgid "Registration address is verified"
msgstr ""

#: apps/staff/password_validation.py:17
msgid "This password need to have chars and numbers."
msgstr ""

#: apps/staff/password_validation.py:22
msgid "Your password need to have chars and numbers."
msgstr ""

#: apps/staff/public_api/views.py:195 apps/trade/public_api/views.py:179
msgid "User order list"
msgstr ""

#: apps/staff/public_api/views.py:217
msgid "User detail"
msgstr ""

#: apps/staff/public_api/views.py:246 apps/trade/models.py:440
msgid "User transactions"
msgstr ""

#: apps/staff/public_api/views.py:264
msgid "Replenish initial data"
msgstr ""

#: apps/staff/public_api/views.py:282
msgid "Withdraw initial data"
msgstr ""

#: apps/staff/tests/views/test_auth_views.py:186 apps/staff/views/auth.py:120
msgid "Already activated account or error"
msgstr ""

#: apps/staff/tests/views/test_auth_views.py:214 apps/staff/views/auth.py:95
msgid "Password reset success."
msgstr ""

#: apps/staff/use_cases.py:59
msgid "Invalid pin code."
msgstr ""

#: apps/staff/use_cases.py:66
msgid "Too many pin code retries for today"
msgstr ""

#: apps/staff/use_cases.py:167
msgid "Invalid token or credentials has been expired."
msgstr ""

#: apps/staff/validators.py:16
msgid ""
"Enter a valid username. This value may contain only English letters, "
"numbers, and @/./+/-/_ characters."
msgstr ""

#: apps/staff/views/auth.py:47
msgid "Registration success header"
msgstr ""

#: apps/staff/views/auth.py:49
msgid "Thanks. Proceed using the link on your email"
msgstr ""

#: apps/staff/views/auth.py:96
msgid "Password reset success text."
msgstr ""

#: apps/staff/views/auth.py:115
msgid "Registration is over"
msgstr ""

#: apps/staff/views/cabinet.py:151
msgid "Change email header success"
msgstr ""

#: apps/staff/views/cabinet.py:152
msgid "Change email text success"
msgstr ""

#: apps/staff/views/cabinet.py:172
msgid "Change password header success"
msgstr ""

#: apps/staff/views/cabinet.py:173
msgid "Change password text success"
msgstr ""

#: apps/staff/views/cabinet.py:200
msgid "Change pin header success"
msgstr ""

#: apps/staff/views/cabinet.py:202
msgid "Thanks. We will respond on your pin code change request"
msgstr ""

#: apps/staff/views/cabinet.py:232
msgid "Upload passport header success"
msgstr ""

#: apps/staff/views/cabinet.py:233
msgid "Upload passport text success"
msgstr ""

#: apps/statistics/apps.py:7
msgid "statistics"
msgstr ""

#: apps/statistics/const.py:11
msgid "Perminute"
msgstr ""

#: apps/statistics/const.py:12
msgid "Perday"
msgstr ""

#: apps/statistics/const.py:13
msgid "Permonth"
msgstr ""

#: apps/statistics/const.py:18
msgid "Login"
msgstr ""

#: apps/statistics/const.py:19
msgid "Wrong pincode"
msgstr ""

#: apps/statistics/models.py:24
msgid "IP address"
msgstr ""

#: apps/statistics/models.py:27
msgid "Country"
msgstr ""

#: apps/statistics/models.py:31 apps/statistics/models.py:109
msgid "Log event"
msgstr ""

#: apps/statistics/models.py:41 apps/statistics/models.py:42
msgid "Login statistics"
msgstr ""

#: apps/statistics/models.py:65
msgid "Timestamp"
msgstr ""

#: apps/statistics/models.py:67
msgid "Interval"
msgstr ""

#: apps/statistics/models.py:70
msgid "Close price"
msgstr ""

#: apps/statistics/models.py:74
msgid "Open price"
msgstr ""

#: apps/statistics/models.py:78
msgid "The highest price"
msgstr ""

#: apps/statistics/models.py:82
msgid "The lowest price"
msgstr ""

#: apps/statistics/models.py:87
msgid "Trade volume"
msgstr ""

#: apps/statistics/models.py:92
msgid "CurrencyPair"
msgstr ""

#: apps/statistics/models.py:97 apps/statistics/models.py:98
msgid "Trade statistics"
msgstr ""

#: apps/stream/consumers.py:55
msgid "Subscription channel is invalid."
msgstr ""

#: apps/stream/consumers.py:65
msgid "Currency pair is invalid"
msgstr ""

#: apps/stream/consumers.py:75
msgid "Channel is a private and requires \"user_token\""
msgstr ""

#: apps/trade/admin.py:41 shared/admin.py:43
msgid "Is closed"
msgstr ""

#: apps/trade/admin.py:55 shared/admin.py:57
msgid "Yes"
msgstr ""

#: apps/trade/admin.py:56 shared/admin.py:58
msgid "No"
msgstr ""

#: apps/trade/api/serializers.py:77 apps/trade/models.py:190
#: apps/trade/models.py:371
msgid "Amount"
msgstr ""

#: apps/trade/api/serializers.py:81 apps/trade/models.py:193
#: apps/trade/models.py:374
msgid "Unit price"
msgstr ""

#: apps/trade/api/serializers.py:167
msgid "Unit price precision must be less or equal \"{}\""
msgstr ""

#: apps/trade/api/serializers.py:199
msgid "There is not enough money on your balance or it is locked."
msgstr ""

#: apps/trade/api/views.py:111
msgid "Order is closed header"
msgstr ""

#: apps/trade/api/views.py:112
msgid "Order is closed text"
msgstr ""

#: apps/trade/api/views.py:117
msgid "Order is closed partially {} header"
msgstr ""

#: apps/trade/api/views.py:120
msgid "Order is closed partially {} text"
msgstr ""

#: apps/trade/api/views.py:124
msgid "Order created header"
msgstr ""

#: apps/trade/api/views.py:125
msgid "Order created text"
msgstr ""

#: apps/trade/api/views.py:133
msgid "User must have a role"
msgstr ""

#: apps/trade/api/views.py:144
msgid "Internal error. Try again"
msgstr ""

#: apps/trade/const.py:11
msgid "Buying"
msgstr ""

#: apps/trade/const.py:12
msgid "Selling"
msgstr ""

#: apps/trade/const.py:16
msgid "Website commision payment"
msgstr ""

#: apps/trade/const.py:17
msgid "Payment for auction lot"
msgstr ""

#: apps/trade/const.py:18
msgid "Order payment"
msgstr ""

#: apps/trade/const.py:19
msgid "Dividends payment"
msgstr ""

#: apps/trade/const.py:20
msgid "Coins listings"
msgstr ""

#: apps/trade/models.py:47
msgid "Abbreviation"
msgstr ""

#: apps/trade/models.py:50
msgid "System name"
msgstr ""

#: apps/trade/models.py:53
msgid "Used in code to identify currencies. Think carefully before changing it"
msgstr ""

#: apps/trade/models.py:61
msgid "Input/output fee"
msgstr ""

#: apps/trade/models.py:68
msgid "Is auction currency"
msgstr ""

#: apps/trade/models.py:71
msgid "Is market currency"
msgstr ""

#: apps/trade/models.py:74
msgid "Numbers after point"
msgstr ""

#: apps/trade/models.py:77
msgid "Can replenish/withdraw money"
msgstr ""

#: apps/trade/models.py:84
msgid "Currencies"
msgstr ""

#: apps/trade/models.py:138
msgid "Currency pair left element"
msgstr ""

#: apps/trade/models.py:144
msgid "Currency pair right element"
msgstr ""

#: apps/trade/models.py:150
msgid "Default currency pair"
msgstr ""

#: apps/trade/models.py:151
msgid "Will be shown as default on trade view and main page"
msgstr ""

#: apps/trade/models.py:158 apps/trade/models.py:238 apps/trade/models.py:387
#: apps/trade/public_api/serializers.py:109
msgid "Currency pair"
msgstr ""

#: apps/trade/models.py:159
msgid "Currency pairs"
msgstr ""

#: apps/trade/models.py:196
msgid "Commision"
msgstr ""

#: apps/trade/models.py:202 apps/trade/models.py:430
msgid "Type"
msgstr ""

#: apps/trade/models.py:205
msgid "Opposite commision"
msgstr ""

#: apps/trade/models.py:215
msgid "Is partial"
msgstr ""

#: apps/trade/models.py:233
msgid "Orders with null value - order to the website."
msgstr ""

#: apps/trade/models.py:245
msgid "Initial amount"
msgstr ""

#: apps/trade/models.py:252
msgid "Inital sum"
msgstr ""

#: apps/trade/models.py:260
msgid "Initial order"
msgstr ""

#: apps/trade/models.py:269 apps/trade/models.py:396
msgid "Exchange glass"
msgstr ""

#: apps/trade/models.py:280
msgid "Trade order"
msgstr ""

#: apps/trade/models.py:281
msgid "Trade orders"
msgstr ""

#: apps/trade/models.py:330 apps/trade/models.py:344
msgid "Partial buying"
msgstr ""

#: apps/trade/models.py:332 apps/trade/models.py:346
msgid "Partial selling"
msgstr ""

#: apps/trade/models.py:334 apps/trade/models.py:348
#, python-brace-format
msgid "Partial {type}"
msgstr ""

#: apps/trade/models.py:380
msgid "Exchange glass type"
msgstr ""

#: apps/trade/models.py:397
msgid "Exchange glasses"
msgstr ""

#: apps/trade/models.py:439
msgid "User transaction"
msgstr ""

#: apps/trade/public_api/serializers.py:132
msgid "Invalid currency pair"
msgstr ""

#: apps/trade/public_api/views.py:190
msgid "New order"
msgstr ""

#: apps/trade/tests/test_models.py:174
msgid "Partial {}"
msgstr ""

#: apps/wallets/entities.py:88 apps/wallets/entities.py:102
msgid "Wallet post request"
msgstr ""

#: apps/wallets/entities.py:93 apps/wallets/entities.py:107
msgid "Sorry. Unexpected error during working with payment system."
msgstr ""

#: apps/wallets/factories.py:111 apps/wallets/factories.py:128
#, python-brace-format
msgid "There is no service for currency {currency}"
msgstr ""

#: apps/wallets/factories.py:144
#, python-brace-format
msgid "There is no service for {currency} currency"
msgstr ""

#: apps/wallets/geopay/serializers.py:32
#: apps/wallets/wayforpay/serializers.py:32
msgid "FIO"
msgstr ""

#: apps/wallets/geopay/serializers.py:34
#: apps/wallets/wayforpay/serializers.py:34
msgid "Not USA citizen"
msgstr ""

#: apps/wallets/models.py:27
msgid "Meta"
msgstr ""

#: apps/wallets/models.py:30
msgid "Generated address"
msgstr ""

#: apps/wallets/models.py:31
msgid "Generated addresses"
msgstr ""

#: apps/wallets/nem/serializers.py:29
msgid "Payment id"
msgstr ""

#: apps/wallets/serializers.py:57
msgid "Not enough money to out"
msgstr ""

#: apps/wallets/serializers.py:61
msgid "Sum must greater or equal zero"
msgstr ""

#: apps/wallets/use_cases.py:105
msgid "Transaction {} is not exists or approved"
msgstr ""

#: apps/wallets/use_cases.py:124
msgid "Transaction {} is not exists"
msgstr ""

#: apps/wallets/wayforpay/entities.py:59
msgid "Replenish UAH on website"
msgstr ""

#: shared/api/fields.py:24
msgid "This is a write only field"
msgstr ""

#: shared/api/fields.py:25
msgid "The secret key is invalid or malformed."
msgstr ""

#: shared/api/fields.py:27
msgid "Please check the checkbox to prove you are not a robot."
msgstr ""

#: shared/api/fields.py:28
msgid "The response is invalid or malformed."
msgstr ""

#: shared/api/fields.py:29
msgid "Bad request."
msgstr ""

#: shared/api/fields.py:30
msgid "Verification has timed out or duplicate."
msgstr ""

#: shared/api/validators.py:13
msgid "This field must be a positive number larger then 0."
msgstr ""

#: shared/apps.py:9
msgid "Shared"
msgstr ""

#: shared/models.py:25
msgid "Updated at"
msgstr ""

#: shared/models.py:38
msgid "Order"
msgstr ""

#: shared/models.py:72
msgid "\"entity_class\" is not specified."
msgstr ""

#: shared/views.py:85
msgid "There are errors in your data. Please fix them."
msgstr ""
